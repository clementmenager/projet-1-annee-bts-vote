<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Vote</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style type="text/css">     
    .container{
      display: flex; 
      justify-content: center;
    }
    .resultats{
      display: flex;
      justify-content: center;
    }
    body{
      background-image: url("man.png"),url("test.png");
      background-repeat: no-repeat,repeat;
      text-align: center;
      background-position:90% 425%,100% 50% ;
    }
    img{
      height: 40%;
      width: 35%;
      position: absolute;
      margin-top: 10%;
      margin-left: -45%;
      z-index: 1
    }
    .all{
      background-color: rgba(249, 206, 51, .5);
      width: 50%;
      height: 50%;
      display: block;
      border:solid 1px;
      margin-left: 25%;  
    }
    </style>
</head>
<body>
  <?php
  function vote(){
    $user="root";
    $mdp="";
    $serveur="localhost";
    $bd="vote"; // annonces
    $dns="mysql:host=$serveur;dbname=$bd";
    $idcon = new PDO($dns, $user, $mdp);
    if(isset($_COOKIE["vote"])){
      echo "<script type='text/javascript'>
      alert('Vous avez voté dans les dix dernières secondes, veuillez patienter !')
      </script>";
    }
    else{
      if(isset($_POST["langage"])){
          $langage = htmlspecialchars($_POST["langage"]);
      $SQL = "INSERT INTO vote VALUES('$langage')";
      $req = $idcon->query($SQL);

      if ($req){
        echo "<script type='text/javascript'>
                          alert('Insertion réussie')
                          </script>";
      } 
      else{  
        echo "<script type='text/javascript'>
                    alert('Problème Insertion')
                    </script>";                   
      }
      setcookie("vote","value",time()+10);
}
}
}
?>
<br><br><br><br>
<div class="all">
<center><h2 class="text-monospace">Veuillez voter ici : </h2></center><br>
    <div class="container">
        <?php vote()?><form class="" action="" method="post">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="langage" value="PHP_MYSQL">
              <label class="form-check-label" for="PHP">
                PHP/MYSQL
              </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="langage" value="ASP_Net">
              <label class="form-check-label" for="ASP">
                ASP.Net
              </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="langage" value="JSP">
              <label class="form-check-label" for="JSP">
                JSP
              </label><br><br>
          </div>
            <button type="submit" name="button" class="btn btn-outline-dark">Voter</button>
        </form>
    </div> <br><br><br>    
    <div class="resultats container">
      <center> <h2 class="text-monospace">Les resultats du vote sont : </h2><br>
      <?php
      $user="root";
      $mdp="";
      $serveur="localhost";
      $bd="vote"; // annonces
      $dns="mysql:host=$serveur;dbname=$bd";
      $idcon = new PDO($dns, $user, $mdp);
      $SQLall = "SELECT COUNT(*)
                 FROM vote";
      $reqall = $idcon->query($SQLall);
      $countall = $reqall->fetchColumn();

      $SQLphp = "SELECT COUNT(*)
                 FROM vote WHERE langage='PHP_MYSQL'";
      $reqphp = $idcon->query($SQLphp);
      $countphp = $reqphp->fetchColumn();
      $pourcentagephp = ($countphp / $countall) * 100;

      $SQLasp = "SELECT COUNT(*)
                 FROM vote WHERE langage='ASP_Net'";
      $reqasp = $idcon->query($SQLasp);
      $countasp = $reqasp->fetchColumn();
      $pourcentageasp = ($countasp / $countall) * 100;

      $SQLjsp = "SELECT COUNT(*)
                 FROM vote WHERE langage='JSP'";
      $reqjsp = $idcon->query($SQLjsp);
      $countjsp = $reqjsp->fetchColumn();
      $pourcentagejsp = ($countjsp / $countall) * 100;
      ?> <div class="container">
      Les résultats pour PHP sont : <?php echo number_format($pourcentagephp,2); ?>%<br>
      Les résultats pour ASP sont : <?php echo number_format($pourcentageasp,2); ?>%<br>
      Les résultats pour JSP sont : <?php echo number_format($pourcentagejsp,2); ?>%<br>
      </div>
     </center> 
 
  </body>
</html>
